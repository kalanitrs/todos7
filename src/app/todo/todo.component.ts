import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TodosService } from '../todos.service'; 

@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})

export class TodoComponent implements OnInit {
  @Input() data:any;
  @Output() myButtonClicked = new EventEmitter<any>();

  text;
  key;
  showButton = false;
  showEditField = false;
  tempText;

  constructor(private todosService:TodosService) { 
  }

  ngOnInit() {
    this.text = this.data.text;
    this.key = this.data.key;
  }

  onClick() {
    this.myButtonClicked.emit(this.text);
  }

  buttonOn() {
    this.showButton = true;
  }

  buttonOff() {
    this.showButton = false;
  }

  delete() {
    this.todosService.deleteTodo(this.key);
  }

  showEdit() {
    this.showEditField = true;
    this.tempText = this.text;
  }
  
  save() {
    this.todosService.update(this.key, this.text);
    this.showEditField = false;
  }

  cancel() {
    this.showEditField = false;
    this.text = this.tempText;
  }
}
