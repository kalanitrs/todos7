import { Component, OnInit } from '@angular/core';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';
import { AuthService } from '../auth.service';
import { TodosService } from '../todos.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  todos = [];
  show = 'no button';
  task: string;

  constructor(private db:AngularFireDatabase, private authService:AuthService, private todosService:TodosService) { }

  ngOnInit() {
    this.authService.user.subscribe(
      user => {
        this.db.list('/users/'+user.uid+'/todos').snapshotChanges().subscribe( // tell me for all the data changes on the things below
          todos => { // the array he took from the FireBase
            this.todos = [];
            todos.forEach(
              todo => {
                let task = todo.payload.toJSON();
                task['key'] = todo.key;
                this.todos.push(task);
              }
            )
          }
        )
      }
    )
  }

  showText(text) {
        this.show = text;
  }

  addTodo() {
    this.todosService.addTodos(this.task);
    this.task = '';
  }
}
