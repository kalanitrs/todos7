import { Component, OnInit } from '@angular/core';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';


@Component({
  selector: 'usertodos',
  templateUrl: './usertodos.component.html',
  styleUrls: ['./usertodos.component.css']
})
export class UsertodosComponent implements OnInit {

  todos = [];
  show = 'no button';
  user = 'Jack';

  constructor(private db:AngularFireDatabase) { }

  ngOnInit() {
    this.db.list('/users/'+this.user+'/todos').snapshotChanges().subscribe( // tell me for all the data changes on the things below
      todos => {
        this.todos = [];
        todos.forEach(
          todo => {
            let y = todo.payload.toJSON();
            y['$key'] = todo.key;
            this.todos.push(y);
          }
        )
      }
    )
  }

  showText(text) {
    this.show = text;
  }

  changeUser() {
    this.db.list('/users/'+this.user+'/todos').snapshotChanges().subscribe( // tell me for all the data changes on the things below
      todos => {
        this.todos = [];
        todos.forEach(
          todo => {
            let y = todo.payload.toJSON();
            y['$key'] = todo.key;
            this.todos.push(y);
          }
        )
      }
    )
  }
}