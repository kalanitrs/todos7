import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  email: string;
  name: string;
  password: string;
  error = "";

  constructor(private authService:AuthService, private router:Router) { }

  ngOnInit() {
  }

  signUp() {
    this.authService.signUp(this.email, this.password)
    .then(value => {
      this.authService.updateProfile(value.user, this.name);
      this.authService.addUser(value.user, this.name);
    }).then(value => {
      this.router.navigate(['/']);
    }).catch(err => {
      this.error = err;
      console.log(this.error);
    })
  }

}
