import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//material design imports
import {MatCardModule} from '@angular/material/card';

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { NavComponent } from './nav/nav.component';
import { TodosComponent } from './todos/todos.component';
import { LoginComponent } from './login/login.component';
import { TodoComponent } from './todo/todo.component';
import { RegistrationComponent } from './registration/registration.component';
import { CodesComponent } from './codes/codes.component';
import { Routes, RouterModule } from '@angular/router';

// fire base
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';

// connection to environment
import { environment } from '../environments/environment';
import { UsertodosComponent } from './usertodos/usertodos.component';

import { FormsModule } from '@angular/forms';

import { MatInputModule} from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NavComponent,
    TodosComponent,
    LoginComponent,
    TodoComponent,
    RegistrationComponent,
    CodesComponent,
    UsertodosComponent
  ],
  imports: [
    BrowserModule,
    MatCardModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    FormsModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatButtonModule,
    RouterModule.forRoot([
      {path:'', component:TodosComponent},
      {path:'register', component:RegistrationComponent},
      {path:'login', component:LoginComponent},
      {path:'codes', component:CodesComponent},
      {path: 'usertodos', component:UsertodosComponent},
      {path:'**', component:TodosComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
