import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase } from '@angular/fire/database';


@Injectable({
  providedIn: 'root'
})
export class TodosService {

  constructor(private authService:AuthService, private db:AngularFireDatabase) { }

  addTodos(todo: string) {
    this.authService.user.subscribe(
      user => {
        this.db.list('/users/'+user.uid+'/todos').push({'text': todo});
      }
    )
  }

  deleteTodo(key: string) {
    this.authService.user.subscribe(
      user => {
        this.db.list('users/'+user.uid+'/todos').remove(key);
      }
    )
  }

  update(key, text) {
    this.authService.user.subscribe(
      user => {
        this.db.list('users/'+user.uid+'/todos').update(key,{'text':text});
      }
    )
  }
}
